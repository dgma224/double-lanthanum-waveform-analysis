#create a dataFile class
#this will hopefully be compatible with the Nab datafile class
#that way I can use them together

import numpy as np
import os
import matplotlib.pyplot as plt

def headerType():
	return {'names': ['head1', 'head2', 'head3', 'daqtime', 'unixtime'], 'formats': ['u4', 'u4', 'u4', 'u4', 'u4']}

def headWaveType(wavelength):
	return {'names': ['header', 'wave'], 'formats': [headerType(), str(wavelength)+'H']}

class dataFile:
	def __init__(self, fileName):
		#name and open the file based on the input
		self.fileName = fileName
		self.__file = open(self.fileName, "rb")
		#define the data types
		self.__headerType = np.dtype(headerType())
		self.wavelength = 500000
		self.__waveType = str(self.wavelength)+'h'
		self.__headWaveType = headWaveType(self.wavelength)
		
		#now load in the file
		#figure out how many "waveforms" are in the file
		self.getFileReadInformation()	
		#load all of these
		self.loadFile()	

	def getFileReadInformation(self):
		size = os.stat(self.fileName).st_size
		self.numWaves = int(size/(self.wavelength * 2 + self.__headerType.itemsize))
	
	def loadFile(self):
		self.__wholeFile = np.fromfile(self.__file, dtype=self.__headWaveType, count = -1)
		#remove the extra bit from the 'daqtime' column
		self.__wholeFile['header']['daqtime'] = self.__wholeFile['header']['daqtime']& (~(1 << 31))		
		self.__heads = self.__wholeFile['header']	
		self.__waves = self.__wholeFile['wave']

	def wave(self, wavenumber):
		return self.__waves[wavenumber]

	def header(self, wavenumber):
		return self.__heads[wavenumber]
	
	def waves(self):
		return self.__waves[:]

	def heads(self):
		return self.__heads[:]

	def wholeFile(self):
		return self.__wholeFile[:]



