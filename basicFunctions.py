import numpy as np
import os,sys
import struct
import matplotlib.pyplot as plt
import math
from scipy import signal
from scipy.stats import norm
from scipy.optimize import curve_fit
from scipy import interpolate
import time
from scipy.signal import find_peaks
from array import array

import pandas as pd
from numba import jit
'''
This is the library of basic functions that are used in waveAnalysis.py.
The intention is that waveAnalysis has all of the real analysis functions and
this file is all of the basic functions to clean up that code.

IMPORTANT NOTE: This file expects to be loaded by the waveAnalysis scripts so nothing
is imported here.
'''

'''
This function does the conversion from 14 bit to 16 bit signed shorts.
'''
def baselineShift(wave, offset):
	mean = np.mean(wave[:offset])
	return wave-mean

'''
This script loads in the results in the format of a binary file.
This can load results from the nearline analysis server.
def binaryResLoad(filename,bc=(-1,-1), fileFormat = 'Nab'):
  #define the data type that will be read in 
	if fileFormat == 'Nab':
		dt = {'names': ['result', 'eventid', 'board', 'channel', 'timestamp', 'length', 'energy', 't0'], 'formats': ['bool', 'i4', 'i4', 'i4', 'i8', 'i4', 'f4', 'f4']
	headerType = data.headerType(fileFormat = fileFormat)
	#since it's a binary file, it's from the trapezoidal filter
	dt=data.flatResultsType(fileFormat = fileFormat)
	#read the file in
	readIn=np.fromfile(filename,dtype=dt,count=-1)
	#now determine what channels to keep based on the bc parameter
	bc=boardChannelConversion(bc)
	if(bc[0]==(-1,-1)): #keep everything
		return readIn
	else:
		#convert bc to a list no matter how many elements it has
		#convert to a pandas dataframe
		if len(bc) == 1: #only one board and channel we can do the pandas dataframe trick	
			output = pd.DataFrame(readIn)	
			output = output[output['board']==bc[0][0]][:]
			output = output[output['channel']==bc[0][1]][:]
			return output.to_records()
		else:	
			output = []
			for i in range(len(readIn)):
				readBC=(readIn[i]['board'],readIn[i]['channel'])
				if readBC in bc:
					output.append(readIn[i])
			results=np.asarray(output, dtype=dt)
			return results

'''

'''
This function loads the results of a previous analysis just like binaryResLoad except for it handles .csv files instead.
def textResLoad(filename,bc=(-1,-1), fileFormat = 'Nab'):
  #use pandas to load the file
	#don't expect a data format, just read it from the file
	readIn = pd.read_csv(filename, sep = ',', skipinitialspace=True)
  #now convert this to a records array for normal numpy usage
	bc=boardChannelConversion(bc)
	if(bc[0] == (-1,-1)):
		return readIn
	readInRec = readIn.to_records()
	output=[]
	for header in readInRec:
		readBC=(header['board'],header['channel'])
		if readBC in bc:
			output.append(header)
	results=np.asarray(output,dtype=readInRec.dtype)
	return results

'''
'''
This function loads in the filters from the filters.dat function
def loadFitFilters(filename):
	filtFile = open(filename, 'r')
	#now grab the information from the file
	firstLine = filtFile.readline().split()
	numFilts = int(firstLine[0])
	filtLength = int(firstLine[1])
	numIdealPulse = int(firstLine[2])

	#load the filters themselves
	origFilters = []
	for i in range(numFilts):
		origFilters.append(np.array(filtFile.readline().split(), dtype=float))
	#now load in the inverse filters
	inverseFilters = []
	for i in range(numFilts):
		inverseFilters.append(np.array(filtFile.readline().split(),dtype=float))
	#now load in the matrix squared term
	filterSquared = []
	for i in range(numFilts):
		filterSquared.append(np.array(filtFile.readline().split(),dtype=float))
	
	return numFilts, filtLength, numIdealPulse, origFilters, inverseFilters, filterSquared	
'''
'''
This is just to determine how many waveforms were present on each pixel.
'''
def getNumWaves(filename,bc=(-1,-1)):
	bc=boardChannelConversion(bc)
	#read in the first waveform to see what the details are
	numwaves, wavelength = getFileReadInformation(filename)
	if bc[0]==(-1,-1):
		return [numwaves]
	else:
		count=np.zeros(len(bc))
		with open(filename,"rb") as f:
			for i in range(int(numwaves)):
				head=f.read(25) #read header
				header=struct.unpack("<?iiiqi",head) #unpack header into container
				head=f.read(wavelength*2) #read waveform but throw it out this time, don't need to store it
				#now see if the board and channel combo is in the list already
				readBC=(int(header[2]),int(header[3]))
				if readBC in bc:
					#get location of the match in bc
					loc=bc.index(readBC)
					count[loc]+=1
	return count


def determineOutputFileName(inFileName, extension):
	#now handle the output file name and open it
	outstart, _ = os.path.splitext(inFileName)
	outname=outstart
	#now handle the boards and channels
	bc=boardChannelConversion(bc)	
	if bc[0]==(-1,-1):
		outname=outstart+extension
	else:
		for a,b in bc:
			outname+='bc'+str(a)+str(b)
		outname+=extension
	return outname

'''
This function outputs histogram information from peakEval as .csv
'''
def printHistCSV(filename,bins,hist):
	fout = open(filename,"w+")
	fout.write('Bins, Count\n')
	for i in range(len(hist)):
		fout.write(str(bins[i])+', '+str(hist[i])+'\n')
	fout.close()	
	return	


'''
These next two functions simply output the results to text files for easy analysis with other programs. 
'''
def outputTrapResults(outfile, num, results):
	#first figure out the output file name
	header=results['header']
	outfile.write(str(int(header['result'])) +', '+ str(int(header['eventid'])) +', '+  str(int(header['board'])) +', '+  str(int(header['channel'])) +', '+  str(int(header['timestamp'])) +', '+  str(int(header['length'])) +', '+  str(float(results['results']['energy'])) +', '+  str(float(results['results']['t0'])) + '\n')
					

def outputFitResults(outfile,num, header, fitparameter):
	#first figure out the output file name
		outfile.write(str(int(header['result'])) +', '+ str(int(header['eventid'])) +', '+  str(int(header['board'])) +', '+  str(int(header['channel'])) +', '+  str(int(header['timestamp'])) +', '+  str(int(header['length'])) +', '+  str(float(fitparameter[0])) +', '+  str(float(fitparameter[1])) + ', ' +str(float(fitparameter[2]))+ ', '+str(float(fitparameter[3]))+ ', '+str(float(fitparameter[4]))+'\n')

def writeCorrectionTable(filename,noiseFile,offset,rise,top,tau,threshold,board,channel,numSim,numStep,fitM,fitB):
	#this function appends to the end of the table the most recent fit parameters
	f = open('fitTable.txt',"a")
	f.write(str(filename)+ ' ' + str(noiseFile) + ' ' + str(offset) + ' '+str(rise) + ' ' + str(top) + ' ' + str(tau) + ' ' + str(threshold) + ' ' + str(board) + ' ' + str(channel) + ' ' + str(numSim) + ' ' + str(numStep) +' ' + str(fitM) + ' ' + str(fitB) + '\n')			
	f.close()



'''
The goal of this function is to output the fit parameters for particular inputs.
This way if you run with the same inputs as a previous run, you don't have to wait as long
Note that the inupts do have to be EXACTLY the same, I don't allow flexibility in this currently.
'''
def checkCorrectionTable(filename,noiseFile,offset,rise,top,tau,threshold,board,channel,numSim,numStep):
	#now search the file for the particular entry we are looking for
	fitM=False
	fitB=False
	exists = os.path.isfile('fitTable.txt')
	if exists:
		#figure out how long the file is
		with open('fitTable.txt',"r") as f:
			lines = f.readlines()
			#get past the two header lines
			#now read the rest of the file
			for i in range(len(lines)):
				line = lines[i].split()
				#check to see if that line is a comment or not
				if line[0]!="#":
					if (str(line[0])==str(filename) and str(line[1])==str(noiseFile) and str(line[2])==str(offset) and str(line[3])==str(rise) and str(line[4])==str(top) and str(line[5])==str(tau) and str(line[6])==str(threshold) and str(line[7])==str(board) and str(line[8])==str(channel) and str(line[9])==str(numSim) and str(line[10])==str(numStep) and fitM==False and fitB==False):
						fitM = float(line[11])
						fitB = float(line[12])
	else:
		f = open('fitTable.txt',"w")
		f.write('# Generated by waveAnalysis.py by the _checkCorrectionTable function\n')
		f.write("# To comment, add a # followed by a space before your comment as done in these lines\n")
		f.write('# Filename noiseFile Offset Rise Top Tau Threshold Board Channel numSim numStep fitM(slope) fitB(yinter)\n')
		f.close()
	return fitM,fitB


'''
This function defines a trapezoidal filter based on the input parameters.
It also scales the trapezoidal filter to properly calculate the energy.
Normally we do the division after the convolution to scale things but it can be done here with no issue as convolution is a linear operation.
Rise = how long in bins the trapezoid will be
Top = how long in bins the top will be (should be always greater than the rise time of the pulse)
Tau = decay constant (again in bins) of the waveform
num = how long the filter should be (always the length of the waveform) 
'''
def defineTrap(rise,top,tau,num): #this function defines the trapezoidal filter to be used
	filt=np.zeros(num)
	for i in range(rise):
		filt[i]=i+tau
		filt[i+rise+top]=rise-tau-i
	for i in range(rise,rise+top):
		filt[i]=rise
	for i in range(rise+rise+top,num):
		filt[i]=0
	scale=1.0/(rise*tau)
	filt*=scale
	return filt




'''
This function does the energy and t0 extraction from the convolved waveforms.
First it finds the maximum energy in the waveform and its location. 
Once that is known, it then determines when the output wave crosses a threshold (defined by the corresponding variable) relative to this max value in both the upwards and downwards direction (both sides of the trapezoid). 
Using this information, this function finds the middle of the trapezoid and extracts the energy from around that location. 
For t0 extraction, we know that the middle of the trapezoid should be half of the top length from the beginning of the trapezoid.
We also know how long the rise time is because that is defined. Using these two values the initial t0 of the trapezoid is found (this shift value is defined in t0shift).

waves=waveforms to be analyzed, it expects a list with 2 or more values
filt = numpy array or python list of the filter itself
t0shift = how far back to move from the midpoint of the trapezoid for the timing (calculated for you in the doTrapAnalysis function, also an integer)
threshold = percent of amplitude to cross over for energy extraction (must be less than 1)
'''
def energyt0(wave,filt,t0shift,threshold): 
	res=signal.fftconvolve(wave,filt,mode='full')[:len(filt)]
	#now find the max value and then the crossing points
	maxval=max(res)
	maxloc=np.argmax(res)
	crossval=threshold*maxval
	j=maxloc
	leftcross=0
	#for the left cross point, start at the maximum value and iterate to the left
	while(j>maxloc-t0shift and j > 1): #add 1 check to make sure it isnt outside the waveform
		if(res[j]>=crossval and res[j-1]<=crossval):
			leftcross=j-0.5
			j=0
		j=j-1
	rightcross=0
	j=maxloc
	while(j<maxloc+t0shift and j < len(filt)-1):
		if(res[j]>=crossval and res[j+1]<=crossval):
			rightcross=j+0.5
			j=maxloc+t0shift
		j=j+1
	midpoint=int((rightcross+leftcross)/2)
	energy=np.mean(res[midpoint:midpoint+5])
	if math.isnan(energy)==True:
		energy = -999999
	t0=midpoint-t0shift
	return energy,t0


'''
This function defines the double exponential used for fitting. 
x = array
amp = amplitude of the pulse
decay1 = fall time of the waveform
decay2 = rise time of the waveform
t0 = when the waveform began 
'''
def doubleExp(x, amp, decay1, decay2, t0):
	y = np.arange(len(x))
	t0=int(t0)
	y = amp * (np.exp(-y/decay1) - np.exp(-y/decay2))
	y = np.roll(y, t0)
	y[0:t0]=0.
	return y

'''
Define a gaussian function for scipy curve_fit usage in the peakCompare function
x=array of x values
a = amplitude
x0 = mean
sigma=standard deviation
'''
def gaus(x,a,x0,sigma):
    return a*np.exp(-(x-x0)**2.0/(2.0*sigma**2.0))


'''
This function simulates the electronics response to a pulse. This was written by Aaron Jezghani
and has been adapted for Python
Currently there is no randomization in the waveform production, that will be added.
'''
def genPulse(amp, length, offset=1000):
	ccSlow = 2.5 #slow charge collection time
	ccSlow = ccSlow/(ccSlow+1)
	ccFast = 1.0/2.5  #fast charge collection time
	alphaCr = 1250/(1250+1) #fall time of output
	alphaRc1 = 1/2.75
	alphaRc2 = 1/2.75
	step=[0,0]
	charge = [0,0]
	curS=[0,0]
	curF=[0,0]
	cr=[0,0]
	rc1 = [0,0]
	rc2 = [0,0]
	t0 = offset
	output = np.zeros(length)
	for i in range(length):
		step[i%2] = 1 if i>=t0 else 0
		curS[i%2]=ccSlow*(curS[(i+1)%2]+step[i%2]-step[(i+1)%2])
		curF[i%2]=ccFast*(curS[i%2]-curF[(i+1)%2])+curF[(i+1)%2]
		charge[i%2]=charge[(i+1)%2]+amp*curF[i%2]*(1./ccSlow-1.)
		cr[i%2]=alphaCr*(cr[(i+1)%2]+charge[i%2]-charge[(i+1)%2])
		rc1[i%2]=alphaRc1*(cr[i%2]-rc1[(i+1)%2])+rc1[(i+1)%2]
		rc2[i%2]=alphaRc2*(rc1[i%2]-rc2[(i+1)%2])+rc2[(i+1)%2]
		output[i]=rc2[i%2]
	return output


'''
This function super-imposes noise onto a given ideal pulse
wave is the ideal pulse
powerSpectrum is the output from getPowerSpectrum
'''
def superimposeNoise(wave, powerSpectrum):
	#first simulate an ideal wave
	#now generate the fake noise
	frequencies = powerSpectrum[0]#the frequency range
	powerSpec = powerSpectrum[1][0]#the power spectra themselves, this function only uses the first one
	rms = np.random.normal(powerSpectrum[2][0], powerSpectrum[3][0]) #determines the RMS of the noise	
	#first make an empty array of complex values
	fftlength = len(powerSpectrum[0]) #powerSpectrum[0] is the frequency range, it is half as long as the source
	noiseFFT = np.zeros(fftlength * 2,dtype=np.complex64)
	#now assign a random phase to each frequency in the spectrum
	amplitudes=np.sqrt(np.multiply(powerSpec,0.5))	
	randPhases = np.random.random(fftlength) * 2.0 * np.pi
	noiseFFT[:fftlength].real = np.cos(randPhases[:])*amplitudes[:]
	noiseFFT[:fftlength].imag = np.sin(randPhases[:])*amplitudes[:]
	noiseFFT[-1:-fftlength-1:-1].real = np.cos(randPhases[:])*amplitudes[:]
	noiseFFT[-1:-fftlength-1:-1].imag = -1.0*np.sin(randPhases[:])*amplitudes[:]
	
	#for i in range(fftlength):
	#	phase = 2*np.pi*np.random.random_sample()
	#	noiseFFT[i] = complex(amplitudes[i]*np.cos(phase),amplitudes[i]*np.sin(phase))
	#	noiseFFT[-i]= complex(amplitudes[i]*np.cos(phase),-1*amplitudes[i]*np.sin(phase))
	noiseFFT[0] = 0.
	noise = np.fft.ifft(noiseFFT)
	#now make sure we made enough noise
	while(len(noise)<len(wave)):
		noise = np.concatenate((noise,noise))
	#now shrink the noise to match the proper length
	noise=noise[:len(wave)]
	#now make sure the RMS is set to the average
	noiseRMS = np.sqrt(np.mean(np.square(noise[:].real)))
	ratio=rms/noiseRMS
	noise = np.multiply(noise[:].real,ratio)
	wave=np.add(wave,noise[:].real)
	return wave


'''
Here is where the fitting is actually done. This uses the scipy library function curve_fit to do the fitting.
For a performance increase, restrict the bounds more on the parameters being given to the fitting. 
For example if you were to fit all of the waveforms from one file and histogram the rise and fall time constants, it is probably safe to just use the mean of those in the future instead of allowing a large range of values (those should be fixed by electronics).
'''
def doFitting(waveform, offset):
	xdata = np.arange(0,len(waveform))
	#the bounds on this fitting can be adjusted as you want
	#the first element represents the (lower then upper) bounds on the amplitude
	#the second element is for the decay constant
	#third is for the rise time constant
	#fourth is for t0
	p0=[np.max(waveform), 1250.0, 10.0, offset]
	popt,pcov = curve_fit(doubleExp, xdata, waveform, p0=p0, bounds=([0,800,0,offset-100],[8192,1600,50,len(waveform)]), maxfev=5000)
	#now calculate R^2 coefficient
	fitWave = doubleExp(xdata, popt[0],popt[1],popt[2],popt[3])
	ss_res=np.sum((waveform-fitWave)**2.0)
	ss_tot = np.sum((waveform-np.mean(waveform))**2.0)
	r2 = np.abs(1 - ss_res / ss_tot)
	output=popt.tolist()
	output.append(r2)
	return output

'''
This next function is the chi-square minimization fitting technique
that uses the pseudo-inverse. The inputs are extensive. This is only meant
to be used by the function in waveAnalysis but can be called otherwise.  
'''
def pseudoChiSquareFitting(wave, filterInfo, X, boxFilter, trapFilt, t0shift, threshold): 
	#unpack the information from filterInfo
	numFilts = filterInfo[0]
	fitLength = filterInfo[1]
	idealPulseShapes = filterInfo[2]
	A = np.array(filterInfo[3])
	Ainv = np.array(filterInfo[4])
	ATA = np.array(filterInfo[5])
	wavelength = len(wave)
	#do the fitting
	for basis in range(Ainv.shape[0]):
		X[basis,:] = signal.convolve(wave, Ainv[basis,:])
	
	ySquared = np.square(wave)
	yTy = signal.convolve(ySquared, boxFilter)
	
	fastMult = (np.dot(np.transpose(X), ATA)*np.transpose(X)).sum(-1)
	chiSquared = yTy[:] - fastMult[:]

	#adding up the fit parameters for the pulse shapes
	idealFit = np.zeros(len(X[0,:]))
	for basis in range(idealPulseShapes):
		idealFit = np.add(idealFit, X[basis,:])
	
	#max location in this array
	waveMaxLoc = np.argmax(idealFit[fitLength:wavelength -1])+fitLength

	#find the minimum chi squared value in the vicinity of the max
	#ideal pulse shape parameter
	minSearchRange = waveMaxLoc-int(fitLength/4) if waveMaxLoc-int(fitLength/4)>=fitLength else fitLength
	maxSearchRange = waveMaxLoc+int(fitLength/4) if waveMaxLoc+int(fitLength/4) <= wavelength - 1 else wavelength-1
	minChiLoc = np.argmin(chiSquared[minSearchRange:maxSearchRange])+minSearchRange

	fitParams = X[:,minChiLoc]
	timeShift = minChiLoc - fitLength
	fakeWavePerf = np.zeros(fitLength)
	for basis in range(idealPulseShapes):
		fakeWavePerf = np.add(fakeWavePerf, A[basis]*fitParams[basis])
	
	energy, t0 = energyt0(fakeWavePerf, trapFilt, t0shift, threshold)

	normalization = fitLength - numFilts
	minChi = chiSquared[minChiLoc]/normalization

	return energy, t0, minChi, fitParams	


'''
This function handles the input and makes it so it is always a list. 
The goal is to make the other functions more compact.
'''
def boardChannelConversion(bc):
	if isinstance(bc,list):
		return bc
	else:
		return [bc,]


'''
This function returns only the information from particular boards and channels. 
It expects the results style input (header + energy and timestamp) and outupts
this format as well, but only the elements that match the boards and channels.
'''
def pickOutBoardChannelFromRes(results,bc):
	#first determine how many instances of the desired board channel combination there are
	output=[]
	for res in results:
		if bc ==(res['board'],res['channel']):
			output.append(res)
	output=np.asarray(output, dtype=results.dtype)
	return output	


def genFakeHead(num, ener, t0, wavelength):
	header = []
	header.append(True)
	header.append(num)
	header.append(t0)
	header.append(0)
	header.append(ener)
	header.append(wavelength)
	return header


def writeHead(f, header):
	f.write(struct.pack('?',header[0]))
	f.write(struct.pack('i',header[1]))
	f.write(struct.pack('i',header[2]))
	f.write(struct.pack('i',header[3]))
	f.write(struct.pack('q',header[4]))
	f.write(struct.pack('i',header[5]))


def writeWave(f, wave):
	wave = array('h', np.asarray(wave, dtype=np.short))
	f.write(wave.tobytes())



def invertOneArray(array):
  squareMag = np.dot(array, array)
  return array/ squareMag

def getPinvScaleFactor():
  #define ideal waveform
  wavelength = 3500
  ideal=doubleExp(np.arange(wavelength),1000,1250,10,int(wavelength/2))
  idealScaled = ideal/np.sum(ideal)
  inverse = invertOneArray(idealScaled)
  inverse = np.flip(inverse)
  result = signal.fftconvolve(ideal)


def simpleFWHM(array):
	#this function finds the full width half max of the peak around the maximum
	maxVal=np.max(array)
	maxLoc=np.argmax(array)
	zeroCrossings = np.where(np.diff(np.sign(array[:]-maxVal/2)))[0]
	return zeroCrossings[1]-zeroCrossings[0]


'''
This function makes the energy histograms for the doPinvAnalysis and doTrapAnalysis functions
'''
def generateHistogram(results, filename, bc=(-1,-1), bins = -1):
	#define the header of the histogram output file
	header='bins, '	
	#create the bins for the histogram
	if not isinstance(bins, np.ndarray):
		bins = np.arange(np.max(results['results']['energy']))		
	#create the matrix for the output
	outCSV=[]
	outCSV.append(bins[:-1]) #output all but the last one
	#now iterate over each board and channel to create the histogram
	histName = determineOutputFileName(filename, bc, '_hist.png')
	csvName = determineOutputFileName(filename, bc, '_hist.csv')
	#handle the board and channel conversion just in case
	bc=boardChannelConversion(bc)
	if bc[0]==(-1,-1):#if doing all at once then do this
		header+='all channels'
		#make the histogram now
		y,x = np.histogram(results['results']['energy'], bins = bins)
		outCSV.append(y)
		plt.hist(results['results']['energy'], bins = bins, histtype = 'step', label = 'all')
		plt.title(filename)
		plt.legend()
		plt.savefig(histName)
		plt.show()
		plt.clf()
	else:
		for b in bc:
			res = pickOutBoardChannelFromRes(results, bc=b)
			y,x = np.histogram(res['results']['energy'], bins = bins)
			outCSV.append(y)
			plt.hist(res['results']['energy'], bins = bins, histtype = 'step', label = str(b))
		plt.title(filename)
		plt.legend()
		plt.savefig(histName)
		plt.show()
		plt.clf()
	#now output the csv	
	outCSV = np.asarray(outCSV)
	np.savetxt(csvName, outCSV, delimiter = ', ', header = header)
	bc = boardChannelConversion


def doAnd(wave):
	wave = wave[:] & 16383
	wave = wave[:] - np.floor(wave[:]/8192)*16384
	return wave


