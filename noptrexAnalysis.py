import numpy as np #basically c++ style vectors/arrays. Dramatic performance increase over standard python lists and whatnot
import matplotlib.pyplot as plt #used for plotting
from scipy.signal import find_peaks #helps find peaks in waveform files

from dataFile import * #my class used to load the files


#the goal of this function is to return a list of all of the peaks for each waveform
#this will be a list of lists (outer = 1 per waveform, inner = # of waveform peaks
def findPeaksInWaveforms(dataFile):
	#parameters for the peak find function
	#these are all optional inputs for the function that we can use
	#to tune the peak detection
	height = None 
	threshold = None 
	distance = 30
	prominence = 50
	width = None
	wlen = None
	rel_height = 0.5
	plateau_size = None
	
	peakX = []
	peakY = []
	for wave in dataFile.waves():
		#in order to use the python peak finding routine, we have to use the negative of each waveform
		#otherwise it doesn't work
		tempWave = -wave[:]
		baseline = np.mean(tempWave[0:1000])
		tempWave = tempWave[:] - baseline
		#look for peaks at least 10 bins apart from each other
		#other parameters are their default values
		peaks, properties = find_peaks(tempWave, height=height, threshold=threshold, distance=distance, prominence=prominence, width=width, wlen=wlen, rel_height=rel_height, plateau_size=plateau_size)
		amps = tempWave[peaks]
		peakX.append(peaks * 10 **(-5))
		peakY.append(amps)
	return peakX, peakY


